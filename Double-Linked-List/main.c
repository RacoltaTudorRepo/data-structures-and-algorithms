#include <stdlib.h>
#include <stdio.h>

typedef struct node {
    int key;
    struct node *next;
    struct node *prev;
} NodeDL;


/* define a List structure, consisting of the addresses
 * (pointers) of the first and last elements
 * */
struct list_header {
    NodeDL *first;
    NodeDL *last;
};


void print_forward(struct list_header L){
    NodeDL *p;
    for(p=L.first; p!=NULL; p=p->next)
        printf("%d ",p->key);
    printf("\n");
}

void print_backward(struct list_header L){
    NodeDL *p;
    for(p=L.last; p!=NULL; p=p->prev)
        printf("%d ",p->key);
    printf("\n");

}

NodeDL *search(struct list_header L, int givenKey){
    NodeDL *p;
    for(p=L.last; p!=NULL; p=p->prev)
        if(p->key==givenKey) return p;
    //TODO insert code here
    return NULL;
}


void insert_first(struct list_header *L, int givenKey){
    NodeDL *p=(NodeDL*)malloc(sizeof(NodeDL));
    p->key=givenKey;
    p->prev=NULL;
    p->next=NULL;
    if(L->first==NULL)
        {L->first=p;
        L->last=p;
        }
     else {
            p->next=L->first;
            L->first->prev=p;
            L->first=p;
          }
}


void insert_last(struct list_header *L, int givenKey){
    NodeDL *p=(NodeDL*)malloc(sizeof(NodeDL));
    p->key=givenKey;
    p->prev=NULL;
    p->next=NULL;
    if(L->first==NULL)
        {L->first=p;
        L->last=p;
        }
        else{
                p->prev=L->last;
                L->last->next=p;
                L->last=p;
            }
    //TODO insert code here
}

void insert_after_key(struct list_header *L, int afterKey, int givenKey){
    NodeDL *p;
    p=(NodeDL*)malloc(sizeof(NodeDL));
    p->key=givenKey;
    p->next=NULL;
    p->prev=NULL;
    NodeDL *q;
    q=L->first;
    while (q->key!=afterKey) q=q->next;
    p->prev = q;
    p->next = q->next;
    if ( q->next != NULL )
        q->next->prev = p;
    q->next = p;
    if ( L->last == q )
        L->last = p;
}

void delete_first(struct list_header *L){
    NodeDL *p;
    if(L->first==NULL) printf("Lista goala");
        else {p=L->first;
             L->first=L->first->next;
             free(p);
             if(L->first==NULL) L->last=NULL;
                else L->first->prev=NULL;
             }
}

void delete_last(struct list_header *L) {
    NodeDL *p;
    p=L->last;
    L->last=L->last->prev;
    if(L->last==NULL) L->first=NULL;
      else L->last->next=NULL;
    free(p);
}

void delete_key(struct list_header *L, int givenKey){
    NodeDL *p;
    for(p=L->last; p!=NULL; p=p->prev)
        if(p->key==givenKey)
            break;
    if(p!=NULL)
    {
        if(p->prev!=NULL) p->prev->next=p->next;
            else L->first=p->next;
        if(p->next!=NULL) p->next->prev=p->prev;
            else L->last=p->prev;
    }
}


int main(){
    /* initialize list to empty list */
    struct list_header L = {NULL, NULL};

    /* test insertion operations */
    /* insert some elements at the beginning */
    insert_first(&L, 3);
    insert_first(&L, 4);
    insert_first(&L, 2);
    insert_first(&L, 1);
    insert_after_key(&L,4,22);

    /* ... and some at the end */
    insert_last(&L, 6);
    insert_last(&L, 8);

    /* print list contents */
    print_forward(L); // 1 2 4 3 6 8
    print_backward(L); // 8 6 3 4 2 1

    /* test search operation */
    //search for two distinct keys...
    int toSearch = 2;
    NodeDL *foundNode = search(L, toSearch);
    if (foundNode == NULL) {
        printf("Node %d NOT found!\n", toSearch);
    } else {
        printf("Node %d found!\n", foundNode->key);
    }

    toSearch = 9;
    foundNode = search(L, toSearch);
    if (foundNode == NULL) {
        printf("Node %d NOT found!\n", toSearch);
    } else {
        printf("Node %d found!\n", foundNode->key);
    }


    /* test deletions */
    delete_first(&L);
    delete_last(&L);
    delete_key(&L, 4);
    delete_key(&L, 15);

    /* print list contents */
    print_forward(L); // 2 3 6
    print_backward(L); // 6 3 2

    delete_key(&L, 2);
    delete_key(&L, 6);

    /* print list contents */
    print_forward(L); // 3
    print_backward(L); // 3

    return 0;
}
