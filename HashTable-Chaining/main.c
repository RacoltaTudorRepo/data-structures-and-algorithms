#include <stdio.h>
#include <stdlib.h> //chaining
#define M 7 //dimensiunea tabelei de dispersie


typedef struct cell{
    int val;
    struct cell *next;
}NodeT;

int hFunction(int value){
    return value%7;
}

int FindElement(NodeT* hTable[M], int Key){
    int x=hFunction(Key);
    NodeT *p=hTable[x];
    while(p!=NULL)
        {if (p->val==Key) return p;
        p=p->next;
        }

  return NULL;
}

void insertElement(NodeT* hTable[M], int key){
    int x=hFunction(key);
    NodeT *p=(NodeT*)malloc(sizeof(NodeT));
    p->val=key;
    p->next=hTable[x]; // p->next = first;
    hTable[x]=p; // first = p
}

void deleteKey(NodeT* hTable[M], int key){
    NodeT *q1,*q;
    q1=NULL;
    q=hTable[hFunction(key)];
    if(q==NULL) printf("Lista este goala la adresa %d\n", hFunction(key));
        else
    if(hTable[hFunction(key)]->val==key)
    {
        hTable[hFunction(key)]=hTable[hFunction(key)]->next;//cap de lista
        free(q);
        return;
    }
    else while(q!=NULL)
        {
            if(q->val==key)
            {
                q1->next=q->next;
                free(q);
                break;
            }
            q1=q;
            q=q->next;
        }
}

//afisarea tuturor elmentelor din tebela de dispersie
void showAll(NodeT* hTable[M]){
    int i;
    for(i = 0; i < M; i++)
    {
        printf(" %d :",i);
        //verificam daca la slotul i am adaugat ceva
        if(hTable[i] != NULL)
        {

            NodeT *p;
            p = hTable[i];
            while (p != NULL)
            {
                printf(" %d ",p->val);
                p = p->next;
            }
        }
        printf("\n");
    }
    printf("\n");
}


int main(){

    // tabela de dispersie
    NodeT* hTable[M];
    int i, n,x;
    //initializam tabelul nostru de dispersie
    for(i = 0; i < M; i++){
        hTable[i] = NULL;
    }

    //inseram un nou element in tabel
    insertElement(hTable, 36);
    insertElement(hTable, 18);
    insertElement(hTable, 6);
    insertElement(hTable, 43);
    insertElement(hTable, 72);

    insertElement(hTable, 10);
    insertElement(hTable, 5);
    insertElement(hTable, 15);

    //afisam toate elementele
    showAll(hTable);
    //stergere
    deleteKey(hTable, 10);
    deleteKey(hTable, 10);

    //afisam toate elementele din tabel din nou
    showAll(hTable);

    return 0;
}

