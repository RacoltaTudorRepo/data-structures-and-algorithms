#include <stdio.h>
#include <conio.h>
#define NMAX 4

int minglobal;
void afisare(int n,int solutie[],int cost_minim)
{
    int i;
    printf("Ciclul hamiltonian minim:\n");
    for (i=0; i<=n; i++)
    {   if(i==0) printf("%d ",solutie[0]);
            else printf("-> %d ",solutie[i]);
    }
    printf("\nCostul minim: %d",cost_minim);
}

void actualizare_ciclu_minim(int n, int a[], int b[])
{
    int i;
    for (i=0; i<=n; i++)
        b[i]=a[i];
}

int verif(int n,int k,int matr[NMAX][NMAX],int sol[])
{
    int i,nod_next;
    for (nod_next=0; nod_next<n; nod_next++) //determinare nod urmator
    {
        int ok=1;
        for (i=0; i<k; i++)
        {
            if (sol[i]==nod_next)
            {
                ok=0;
            }
        }
        if (ok==1) return nod_next;
    }
    return -1;
}

int verif_ciclu(int n,int a[]) //verificare a solutiei partiale, adica daca toate nodurile sunt distincte (ciclu Hamiltonian)
{
    int i,j,ok=1;
    for (i=0; i<n-1; i++)
    {
        for (j=i+1; j<n; j++)
        {
            if (a[i]==a[j])
            {
                ok=0;
            }
        }
    }
    if ((ok==1)&&(a[0]==a[n])) return 1;
    else return 0;
}

int suma(int n,int a[],int matrice[NMAX][NMAX])
{
    int i,s=0;
    for (i=0; i<n; i++)
    {
        s+=matrice[a[i]][a[i+1]]; //costul total prin insumarea fiecarei muchii din solutie
    }
    return s;
}

void backtrack(int k,int n_c,int n,int matr[NMAX][NMAX],int sol[],int cicl_min[])   //n_c = nod curent
{   static int cost_min=9999999;
    int j;
    for (j=n_c; j<n; j++)
    {
        sol[k]=j; //solutie partiala, folosita pentru creeare de ciclu
        if (k<=n)
        {
            int phi;
            if ((k==n)&&(sol[0]==sol[n])&&(phi=verif_ciclu(n,sol)==1)) //Solutia va fi formata din 4 componente distincte, iar ultima pozitie va fi chiar cel de start
            {                                       //de aceea vom testa si daca k==n cu k pornind de la 0
                int x=suma(n,sol,matr);
                if (x<cost_min)
                {
                    cost_min=x;
                    minglobal=x;
                    actualizare_ciclu_minim(n,sol,cicl_min); //solutia curenta devine ciclul minim
                }
            }
            else if (k<n-1)
            {
                n_c=verif(n,k,matr,sol);
                backtrack(k+1,n_c,n,matr,sol,cicl_min); //daca nu este ultima pozitie din solutie, cautam noduri noi, diferite de primul
            }
            else if (k==n-1)
            {
                n_c=sol[0];
                backtrack(k+1,n_c,n,matr,sol,cicl_min); //daca ajungem la ultima pozitie, punem nodul de start
            }
        }
    }
}

int main()
{
    int n=4;  //nr noduri
    int matrice_costuri[NMAX][NMAX]= {   {0,5,11,7},
                                         {5,0,1,6},
                                         {11,1,0,3},
                                         {7,6,3,0},
                                    };
    int sol[6],ciclu_minim[6];
    int i;
    for (i=0; i<=n; i++)
    {
        sol[i]=0;
    }
    backtrack(0,0,n,matrice_costuri,sol,ciclu_minim);
    afisare(n,ciclu_minim,minglobal);
    return 0;
}
