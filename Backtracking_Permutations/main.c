#include <stdio.h>
#include <stdlib.h>

int x[10];
int n=4;

int phi (int k){
    int i;
    for (i=1;i<k;i++){
        if(x[k]==x[i])
            return 0;
    }
    return 1;

}

void print(){
    int i;
    for (i=1;i<=n;i++)
        printf("%d ",x[i]);
    printf("\n");
}

void backtracking (int k){
    int i;
    for (i=1;i<=n;i++){
        x[k]=i;
        if (phi(k)){
            if(k==n)
                print(x);
            else
                backtracking(k+1);
        }
    }
}




int main()
{
    backtracking(1);
    return 0;
}

