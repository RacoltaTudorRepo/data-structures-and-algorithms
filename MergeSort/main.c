#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define NDEBUG

void interclas(int *a, int p, int m, int q)
{
    int x = p, y = m + 1, j = 0;
    int b[100];
    while (x <= m && y <= q)
    {
        if (a[x] > a[y])
                {b[j] = a[y];
                y++;
                }
            else {b[j] = a[x];
                 x++;
                 }
        j++;
    }

    while (x <= m)
        b[j++] = a[x++];
    while (y <= q)
        b[j++] = a[y++];
    for (int i = 0; i < j; i++) //se inlocuieste sirul A cu sirul B interclasat
        {a[p] = b[i];
        p++;
        }
}

void merge_sort(int *a, int p, int q)
{
    int m = (p + q) / 2;
    if (p == q)
        return;
    merge_sort(a, p, m);
    merge_sort(a, m + 1, q);
    interclas(a, p, m, q);
}

int main()
{
    int numbers[] = {4, 9, 3, 1,2};
    int expected[] = {1,3, 4, 5, 6, 7, 8, 9};

    int n = (int) (sizeof(numbers) / sizeof(int));
    merge_sort(numbers, 0, n-1);
    for (int i = 0; i < n; i++)
    {
        printf("%d ", numbers[i]);
        //assert(numbers[i] == expected[i]);
    }
    return 0;
}
