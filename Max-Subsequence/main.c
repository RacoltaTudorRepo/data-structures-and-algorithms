#include <stdio.h>
int n;
int nr, i;
int st, poz, sol, SMax, dr;
int main()
{
 FILE *f = fopen ("secv.txt","r");
 fscanf (f,"%d", &n);
 sol = -0x3f3f3f3f;  /* sol este -Infinit */
 for (i=1;i<=n;++i)
 {

     fscanf (f,"%d", &nr);
     if (SMax < 0)
     {
         SMax = nr;
         poz = i;
     }
     else
         SMax += nr;
     if (SMax > sol)
     {
         sol = SMax;
         st = poz;
         dr = i;
     }
 }
 fclose(f);

 printf ("Suma maxima = %d\n", sol);
 printf ("Pozitia de inceput a secventei %d , pozitia se sfarsit a secventei %d", st, dr);

 return 0;
}
